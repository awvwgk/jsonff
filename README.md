jsonff
=====

[![pipeline status](https://gitlab.com/everythingfunctional/jsonff/badges/main/pipeline.svg)](https://gitlab.com/everythingfunctional/jsonff/commits/main)

JSON for Fortran.

The constants `JSON_NULL`, `JSON_TRUE` and `JSON_FALSE` and the constructors
`json_number_t`, `fallible_json_string_t`, `json_array_t`, `json_member_t` and `json_object_t` are
provided for building up JSON data structures in Fortran. Note that the procedure
to create a `json_string_t` ensures that the string is valid according to the JSON
standard. *Unsafe* versions of the json string, member, and object
procedures are provided in the event you are certain that you are only dealing
with valid strings.

Once constructed, JSON values can be converted to string representation in
either compact or expanded, human-readable formats. Additionally, procedures are
provided (`parse_json`) that parse a string into a JSON data structure. It also
provides reasonable error messages in the event the string does not contain
valid JSON.

A string generated from a valid JSON data structure is guaranteed to be able to
be parsed by the parser into exactly the same data structure. Note that a data
structure parsed from a string is not necessarily guaranteed to produce exactly
the same string, since formatting is not important to JSON data.
