module parse_json_test
    use erloff, only: error_list_t
    use iso_varying_string, only: VARYING_STRING, operator(//), put, var_str
    use jsonff, only: &
            fallible_json_value_t, &
            json_array_t, &
            json_element_t, &
            json_false_t, &
            json_null_t, &
            json_number_t, &
            json_object_t, &
            json_string_t, &
            json_true_t, &
            parse_json, &
            parse_json_from_file
    use strff, only: NEWLINE
    use vegetables, only: &
            example_t, &
            Input_t, &
            Result_t, &
            test_item_t, &
            assert_equals, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it, &
            succeed

    implicit none
    private

    type, extends(Input_t) :: NumberInput_t
        type(VARYING_STRING) :: string
        double precision :: value_
    end type NumberInput_t

    character(len=*), parameter :: EXPANDED_EXAMPLE = &
   '{' // NEWLINE &
// '    "glossary" : {' // NEWLINE &
// '        "title" : "example glossary",' // NEWLINE &
// '        "GlossDiv" : {' // NEWLINE &
// '            "title" : "S",' // NEWLINE &
// '            "GlossList" : {' // NEWLINE &
// '                "GlossEntry" : {' // NEWLINE &
// '                    "ID" : "SGML",' // NEWLINE &
// '                    "SortAs" : "SGML",' // NEWLINE &
// '                    "GlossTerm" : "Standard Generalized Markup Language",' // NEWLINE &
// '                    "Acronym" : "SGML",' // NEWLINE &
// '                    "Abbrev" : "ISO 8879:1986",' // NEWLINE &
// '                    "GlossDef" : {' // NEWLINE &
// '                        "para" : "A meta-markup language, used to create markup languages such as DocBook.",' // NEWLINE &
// '                        "GlossSeeAlso" : [' // NEWLINE &
// '                            "GML",' // NEWLINE &
// '                            "XML"' // NEWLINE &
// '                        ]' // NEWLINE &
// '                    },' // NEWLINE &
// '                    "GlossSee" : "markup"' // NEWLINE &
// '                }' // NEWLINE &
// '            }' // NEWLINE &
// '        }' // NEWLINE &
// '    }' // NEWLINE &
// '}'

    public :: test_parse_json
contains
    function test_parse_json() result(tests)
        type(test_item_t) :: tests

        type(test_item_t) :: individual_tests(14)
        type(example_t) :: number_examples(8)

        number_examples(1) = example_t(NumberInput_t(var_str("0"), 0.0d0))
        number_examples(2) = example_t(NumberInput_t(var_str("2"), 2.0d0))
        number_examples(3) = example_t(NumberInput_t(var_str("-2.0"), -2.0d0))
        number_examples(4) = example_t(NumberInput_t(var_str("0.2"), 0.2d0))
        number_examples(5) = example_t(NumberInput_t(var_str("1.2e3"), 1.2d3))
        number_examples(6) = example_t(NumberInput_t(var_str("1.2E+3"), 1.2d3))
        number_examples(7) = example_t(NumberInput_t(var_str("1.2e-3"), 1.2d-3))
        number_examples(8) = example_t(NumberInput_t(var_str("20e1"), 20.0d1))

        individual_tests(1) = it( &
                "parsing an empty string returns an error", checkParseEmpty)
        individual_tests(2) = it("can parse null", checkParseNull)
        individual_tests(3) = it("can parse true", checkParseTrue)
        individual_tests(4) = it("can parse false", checkParseFalse)
        individual_tests(5) = it( &
                "can parse a variety of numbers", &
                number_examples, &
                checkParseNumber)
        individual_tests(6) = it("can parse a string", checkParseString)
        individual_tests(7) = it( &
                "can parse an empty array", checkParseEmptyArray)
        individual_tests(8) = it( &
                "can parse an array with a single element", &
                checkParseSingleArray)
        individual_tests(9) = it( &
                "can parse an array with multiple elements", &
                checkParseMultiArray)
        individual_tests(10) = it( &
                "can parse an empty object", checkParseEmptyObject)
        individual_tests(11) = it( &
                "can parse an object with a single member", &
                checkParseSingleObject)
        individual_tests(12) = it( &
                "can parse an object with multiple members", &
                checkParseMultiObject)
        individual_tests(13) = it( &
                "can parse complex data", checkParseExpandedJson)
        individual_tests(14) = it( &
                "can parse data from a file", checkParseFromFile)
        tests = describe("parse_json", individual_tests)
    end function test_parse_json

    function checkParseEmpty() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("")
        errors = json%errors()

        result_ = assert_that(errors%has_any(), errors%to_string())
    end function checkParseEmpty

    function checkParseNull() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(" null ")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_null_t)
                result_ = succeed("Got null")
            end select
        end if
    end function checkParseNull

    function checkParseTrue() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(" true ")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_true_t)
                result_ = succeed("Got true")
            end select
        end if
    end function checkParseTrue

    function checkParseFalse() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(" false ")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_false_t)
                result_ = succeed("Got false")
            end select
        end if
    end function checkParseFalse

    function checkParseNumber(input) result(result_)
        class(Input_t), intent(in) :: input
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        select type (input)
        type is (NumberInput_t)
            json = parse_json(input%string)
            errors = json%errors()
            result_ = assert_not(errors%has_any(), errors%to_string())
            if (result_%passed()) then
                select type (element => json%value_())
                type is (json_number_t)
                    result_ = assert_equals( &
                            input%value_, &
                            element%get_value(), &
                            "Original string: " // input%string)
                end select
            end if
        class default
            result_ = fail("Expected to get a NumberInput_t")
        end select
    end function checkParseNumber

    function checkParseString() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: THE_STRING = &
                '"AB\"\\\/\b\n\r\t\u1a2f"'
        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(THE_STRING)
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_string_t)
                result_ = assert_equals(THE_STRING, element%to_compact_string())
            end select
        end if
    end function checkParseString

    function checkParseEmptyArray() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("[ ]")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_array_t)
                result_ = assert_equals(0, element%length())
            end select
        end if
    end function checkParseEmptyArray

    function checkParseSingleArray() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("[20e1]")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals("[200.0]", json_%to_compact_string())
            end associate
        end if
    end function checkParseSingleArray

    function checkParseMultiArray() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("[null, null,null]")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals("[null,null,null]", json_%to_compact_string())
            end associate
        end if
    end function checkParseMultiArray

    function checkParseEmptyObject() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("{ }")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals("{}", json_%to_compact_string())
            end associate
        end if
    end function checkParseEmptyObject

    function checkParseSingleObject() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json('{"first" : null}')
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals('{"first":null}', json_%to_compact_string())
            end associate
        end if
    end function checkParseSingleObject

    function checkParseMultiObject() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json('{"first" : null, "second" : null, "third" : null}')
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals( &
                        '{"first":null,"second":null,"third":null}', &
                        json_%to_compact_string())
            end associate
        end if
    end function checkParseMultiObject

    function checkParseExpandedJson() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(EXPANDED_EXAMPLE)
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals(EXPANDED_EXAMPLE, json_%to_expanded_string())
            end associate
        end if
    end function checkParseExpandedJson

    function checkParseFromFile() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: TEMP_FILE_NAME = "temp_file.json"
        type(error_list_t) :: errors
        integer :: file_unit
        type(fallible_json_value_t) :: json

        open(newunit = file_unit, file = TEMP_FILE_NAME, action = "WRITE", status = "REPLACE")
        call put(file_unit, EXPANDED_EXAMPLE)
        close(file_unit)

        json = parse_json_from_file(TEMP_FILE_NAME)
        errors = json%errors()

        open(newunit = file_unit, file = TEMP_FILE_NAME)
        close(file_unit, status = "DELETE")

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals(EXPANDED_EXAMPLE, json_%to_expanded_string())
            end associate
        end if
    end function checkParseFromFile
end module parse_json_test
