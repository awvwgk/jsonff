module json_test
    use erloff, only: error_list_t, NOT_FOUND, OUT_OF_BOUNDS
    use iso_varying_string, only: VARYING_STRING
    use jsonff, only: &
            fallible_json_string_t, &
            fallible_json_value_t, &
            json_array_t, &
            json_element_t, &
            json_number_t, &
            json_object_t, &
            json_string_t, &
            json_string_unsafe, &
            INVALID_INPUT, &
            JSON_FALSE, &
            JSON_NULL, &
            JSON_TRUE
    use strff, only: NEWLINE
    use vegetables, only: &
            Result_t, &
            test_item_t, &
            assert_equals, &
            assert_includes, &
            assert_not, &
            assert_that, &
            Describe, &
            It

    implicit none
    private

    public :: test_json
contains
    function test_json() result(tests)
        type(test_item_t) :: tests

        type(test_item_t) :: individual_tests(14)

        individual_tests(1) = It( &
                "null has the correct string representation", &
                checkNullToString)
        individual_tests(2) = It( &
                "true has the correct string representation", &
                checkTrueToString)
        individual_tests(3) = It( &
                "false has the correct string representation", &
                checkFalseToString)
        individual_tests(4) = It( &
                "a string can be converted back", checkStringToString)
        individual_tests(5) = It( &
                "a number has the correct string representation", &
                checkNumberToString)
        individual_tests(6) = It( &
                "an array has the correct string representation", &
                checkArrayToString)
        individual_tests(7) = It( &
                "an object has the correct string representation", &
                checkObjectToString)
        individual_tests(8) = It( &
                "a complex object has the correct string representation", &
                checkComplexObjectToString)
        individual_tests(9) = It( &
                "can be generated in an expaned, easier to read form", &
                checkPrettyPrinting)
        individual_tests(10) = It( &
                "can extract a value from an array", getValueFromArray)
        individual_tests(11) = It( &
                "extracting a value from an array with a position greater than the number of elements in the array is an error", &
                getValueFromArrayFailure)
        individual_tests(12) = It( &
                "can extract a value from an object", getValueFromObject)
        individual_tests(13) = It( &
                "extracting a value from an object with a key it doesn't have is an error", &
                getValueFromObjectFailure)
        individual_tests(14) = It( &
                "trying to create an invalid string is an error", &
                checkStringError)
        tests = Describe("JSON", individual_tests)
    end function test_json

    pure function checkNullToString() result(result_)
        type(Result_t) :: result_

        result_ = assert_equals("null", JSON_NULL%to_compact_string())
    end function checkNullToString

    pure function checkTrueToString() result(result_)
        type(Result_t) :: result_

        result_ = assert_equals("true", JSON_TRUE%to_compact_string())
    end function checkTrueToString

    pure function checkFalseToString() result(result_)
        type(Result_t) :: result_

        result_ = assert_equals("false", JSON_FALSE%to_compact_string())
    end function checkFalseToString

    pure function checkStringToString() result(result_)
        type(Result_t) :: result_

        type(json_string_t) :: string

        string = json_string_unsafe("Hello")

        result_ = assert_equals('"Hello"', string%to_compact_string())
    end function checkStringToString

    pure function checkNumberToString() result(result_)
        type(Result_t) :: result_

        type(json_number_t) :: number

        number = json_number_t(1.0d0)

        result_ = assert_equals("1.0", number%to_compact_string())
    end function checkNumberToString

    function checkArrayToString() result(result_)
        type(Result_t) :: result_

        type(json_array_t) :: array

        call array%append(JSON_NULL)
        call array%append(json_string_unsafe("Hello"))
        call array%append(json_number_t(2.0d0))
        result_ = assert_equals('[null,"Hello",2.0]', array%to_compact_string())
    end function checkArrayToString

    function checkObjectToString() result(result_)
        type(Result_t) :: result_

        type(json_object_t) :: object
        type(VARYING_STRING) :: string

        call object%add_unsafe("sayHello", JSON_TRUE)
        call object%add_unsafe("aNumber", json_number_t(3.0d0))
        string = object%to_compact_string()

        result_ = &
                assert_includes('"sayHello":true', string) &
                .and.assert_includes('"aNumber":3.0', string) &
                .and.assert_includes("{", string) &
                .and.assert_includes("}", string) &
                .and.assert_includes(",", string)
    end function checkObjectToString

    function checkComplexObjectToString() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: EXPECTED = &
                '{"Hello":[null,{"World":1.0},true]}'
        type(json_array_t) :: array
        type(json_object_t) :: inner_object
        type(json_object_t) :: outer_object

        call inner_object%add_unsafe("World", json_number_t(1.0d0))
        call array%append(JSON_NULL)
        call array%append(inner_object)
        call array%append(JSON_TRUE)
        call outer_object%add_unsafe("Hello", array)

        result_ = assert_equals(EXPECTED, outer_object%to_compact_string())
    end function checkComplexObjectToString

    function checkPrettyPrinting() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: EXPECTED = &
   '{' // NEWLINE &
// '    "Hello" : [' // NEWLINE &
// '        null,' // NEWLINE &
// '        {' // NEWLINE &
// '            "World" : 1.0' // NEWLINE &
// '        },' // NEWLINE &
// '        true' // NEWLINE &
// '    ]' // NEWLINE &
// '}'
        type(json_array_t) :: array
        type(json_object_t) :: inner_object
        type(json_object_t) :: outer_object

        call inner_object%add_unsafe("World", json_number_t(1.0d0))
        call array%append(JSON_NULL)
        call array%append(inner_object)
        call array%append(JSON_TRUE)
        call outer_object%add_unsafe("Hello", array)

        result_ = assert_equals(EXPECTED, outer_object%to_expanded_string())
    end function checkPrettyPrinting

    function getValueFromArray() result(result_)
        type(Result_t) :: result_

        type(json_array_t) :: array
        type(error_list_t) :: errors
        type(fallible_json_value_t) :: retrieved

        call array%append(json_string_unsafe("first"))
        call array%append(json_string_unsafe("second"))
        call array%append(json_string_unsafe("third"))

        retrieved = array%get_element(3)
        errors = retrieved%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(retrieved_ => retrieved%value_())
                result_ = assert_equals( &
                        '"third"', retrieved_%to_compact_string())
            end associate
        end if
    end function getValueFromArray

    function getValueFromArrayFailure() result(result_)
        type(Result_t) :: result_

        type(json_array_t) :: array
        type(error_list_t) :: errors
        type(fallible_json_value_t) :: retrieved

        call array%append(json_string_unsafe("first"))
        call array%append(json_string_unsafe("second"))
        call array%append(json_string_unsafe("third"))

        retrieved = array%get_element(4)
        errors = retrieved%errors()

        result_ = assert_that( &
                errors.hasType.OUT_OF_BOUNDS, errors%to_string())
    end function getValueFromArrayFailure

    function getValueFromObject() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(json_object_t) :: object
        type(fallible_json_value_t) :: retrieved

        call object%add_unsafe("first", json_string_unsafe("hello"))
        call object%add_unsafe("second", json_string_unsafe("goodbye"))

        retrieved = object%get_element("first")
        errors = retrieved%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(retrieved_ => retrieved%value_())
                result_ = assert_equals( &
                        '"hello"', retrieved_%to_compact_string())
            end associate
        end if
    end function getValueFromObject

    function getValueFromObjectFailure() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(json_object_t) :: object
        type(fallible_json_value_t) :: retrieved

        call object%add_unsafe("first", json_string_unsafe("hello"))
        call object%add_unsafe("second", json_string_unsafe("goodbye"))

        retrieved = object%get_element("third")
        errors = retrieved%errors()

        result_ = assert_that(errors.hasType.NOT_FOUND, errors%to_string())
    end function getValueFromObjectFailure

    function checkStringError() result(result_)
        type(Result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_string_t) :: string

        string = fallible_json_string_t("\invalid")
        errors = string%errors()

        result_ = assert_that(errors.hasType.INVALID_INPUT, errors%to_string())
    end function checkStringError
end module json_test
